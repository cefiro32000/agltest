﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;


namespace AGL.Models
{
    public class OwnerModel
    {
        public string name { get; set; }
        public string gender { get; set; }
        public string age { get; set; }
        //public List<pet> pets { get; set; }
        public List<pet> pets;
    }
    public class pet
    {
        public string name { get; set; }
        public string type { get; set; }
    }

}