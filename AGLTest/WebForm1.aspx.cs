﻿using AGL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Script.Serialization;
using System.Web.UI.WebControls;


namespace AGLTest
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string jsonURL = "http://agl-developer-test.azurewebsites.net";
            List<OwnerModel> owner = GetJsonObject(jsonURL);
            // Male               
            output.Text = "Male" + Environment.NewLine;
            // fetch all the pet as List<List<pet>> type and order by alphebatically
            var maleResult = OwnerCatlist(owner, "Male");
                    
            foreach (pet p in maleResult)
            {
                output2.Text += "<div>";
                output2.Text += "\u2022" + Environment.NewLine + p.name.ToString();
                output2.Text += "</div>";
            }

            // Female
            output3.Text = "Female" + Environment.NewLine;
            // fetch all the pet as List<List<pet>> type and order by alphebatically
            var femaleResult = OwnerCatlist(owner, "Female");
            // display the pet name in alphebtical order
            foreach (pet p in femaleResult)
            {
                output4.Text += "<div>";
                output4.Text += "\u2022" + Environment.NewLine + p.name.ToString();
                output4.Text += "</div>";
            }
        }

        /// <summary>
        /// Utilize HttpCient to retrieve json object.
        /// </summary>
        /// <returns>List<OwnerModel></returns>
        private List<OwnerModel> GetJsonObject(string url)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(url);

            // Add an Accept header for JSON format.
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));

            HttpResponseMessage response = client.GetAsync("people.json").Result;

            if (response.IsSuccessStatusCode)
            {
                var serializer = new JavaScriptSerializer();
                // deserialize json object -> convert and map to .NET OwnerModel object 
                List<OwnerModel> owner = serializer.Deserialize<List<OwnerModel>>(response.Content.ReadAsStringAsync().Result);
                return owner;
            }
            else
            {
                throw new Exception("Success Status Code is not returned.");
            }
        }

        /// <summary>
        /// To return the cat list based on gender using LINQ.
        /// This function can be extended by supply another parameters to fetch pets type.
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="gender"></param>
        /// <returns>List<list<pet>></returns>
        private List<pet> OwnerCatlist(List<OwnerModel> owner, string gender)
        {
            // extract the list of owner's pet with type of 'Cat' and return a list of pet in alphabetically order
            return owner
                .Where(p => p.gender.Equals(gender))
                .Where(p => p.pets != null)
                .Select(temp => temp.pets.FindAll(x => x.type == "Cat"))
                .SelectMany(x => x)
                .OrderBy(y => y.name).ToList();
        }
    }
}